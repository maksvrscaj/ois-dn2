var http = require('http');
var fs  = require('fs');
var path = require('path');
var mime = require('mime');
var predpomnilnik = {};

function posredujNapako404(odgovor) { 
  odgovor.writeHead(404, {'Content-Type': 'text/plain'});
  odgovor.write('Napaka 404: Vira ni mogoče najti.');
  odgovor.end();
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina) { //odgovor vrnemo, naloga: da ugotovimo kontent type
  odgovor.writeHead(200, {"content-type": mime.lookup(path.basename(datotekaPot))});
  odgovor.end(datotekaVsebina);
}

function posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke) { //klice, ko pride zahteva s strani odjemalca
  if (predpomnilnik[absolutnaPotDoDatoteke]) { // ce jo ze mamo v predpomnilniku
    posredujDatoteko(odgovor, absolutnaPotDoDatoteke, predpomnilnik[absolutnaPotDoDatoteke]);
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {// ce datoteka obstaja
      if (datotekaObstaja) { //preberemo datoteko
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            posredujNapako404(odgovor);
          } else { //shranimo v predpomnilnik, to je hash map, dodatmo absolutno pot, kot vrednost vsebina
            predpomnilnik[absolutnaPotDoDatoteke] = datotekaVsebina;
            posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina); //vrnemo kot odgovor
          }
        });
      } else {
        posredujNapako404(odgovor);
      }
    });
  }
}

var streznik = http.createServer(function(zahteva, odgovor) { //ustvarimo streznik
  var potDoDatoteke = false;

  if (zahteva.url == '/') {//zahteva na root , npr http://www.fri.uni-lj.si/
    potDoDatoteke = 'public/index.html';
  } else { //druga zahteva
    potDoDatoteke = 'public' + zahteva.url; //uzames pot npr. : ...si/.../.../a.txt    
  }

  var absolutnaPotDoDatoteke = './' + potDoDatoteke;
  posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke);
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik posluša na portu " + process.env.PORT + ".");//ip ni potreben ker ze privzeto
});


var klepetalnicaStreznik = require('./lib/klepetalnica_streznik'); //to dodamo, ko zelimo dodati dinamiko. 
//Objekt klepetalnicaStreznik z ukazom dodamo nasemu obstojecemu strezniku. Kar dodajamo.
klepetalnicaStreznik.listen(streznik);