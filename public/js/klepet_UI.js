function divElementEnostavniTekst(sporocilo) {/* dve funkciji da obvladujemo napade ce namesto sporocila vnesem <script>alert('xss napad!');</script> vsem uporabnikom bi se izpisalo okence z xss napad. Dovolj je, da taga shranjujemo in da se znaki ne izvedejo, ampak nadomestijo z < &LT; koncni> pa z &GT*/ 

  
  sporocilo = sporocilo.split(";)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
  sporocilo = sporocilo.split(":)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
  sporocilo = sporocilo.split("(y)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
  sporocilo = sporocilo.split(":*").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
  sporocilo = sporocilo.split(":(").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
  
  return $('<div style="font-weight: bold"></div>').html(sporocilo);



}

function divElementHtmlTekst(sporocilo) {/* ukazi, ki so posledica sistemsko generiranih zahrev */
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {/* na strani odjemalca*/
  var sporocilo = $('#poslji-sporocilo').val();/* dobimo vrednost ki jo vnese user. Ce se sporocilo ne zacne s slashom, potem gre za sporocilo in to sporocilo sprocesiramo kot ukaz in ga posjemo kot html dokument */
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    } 
  }
  else {/* posljemo naprej kot tekst */
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(filtriraj(sporocilo)));/*pripnemo na dno sporocil */

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));/* poskrolamo dol */
  }

  $('#poslji-sporocilo').val('');/* izbrisemo to kar je user vnesel*/
}

function filtriraj(sporocilo){
  var seznamVulgarnih=$('#vulgarizmi').text().split("\n");
   for(var i=0;i<seznamVulgarnih.length;i++){
        var tmp=seznamVulgarnih[i];
        var dolzina=tmp.length;
        var niz="";
        for(var j=0;j<dolzina;j++){
         niz+="*";
        }
        sporocilo=sporocilo.split(seznamVulgarnih[i]).join(niz);
    }
    return sporocilo;
    
}

var socket = io.connect();/*odpremo socket */

$(document).ready(function() { /* To se izvrsi takrat, ko se celoten dokument nalozi, sele potem zacnemo z izvajanjem logike, ker se lahko se vedno loada ko ze pisemo, zato pocakamo da je koncna verzija*/
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {/* poslusamo na kanalu vSO in ko dobimo odgovor izpisemo sporocilo Prijavljen si kot ...*/
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $("#seznam-uporabnikov").append(divElementHtmlTekst(rezultat.vzdevek));
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#kanal').text(rezultat.vzdevek+ " @ " + rezultat.kanal);/* updatamo tekst*/
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {/* pocakamo na odgovor streznika*/

    $('#vulgarizmi').load("swearWords.txt");
    $('#kanal').text(rezultat.kanal);/* updatamo tekst*/

    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  

  socket.on('sporocilo', function (sporocilo) {/* poslusamo na sporociloh*/
    var novElement = $('<div style="font-weight: bold"></div>').text(filtriraj(sporocilo.besedilo));/* dodamo ga*/
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {/* skrbi za azuren seznam kanalov ki so na voljo*/
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() { /* ce imamo vec kanalov kliknemo na kanal in nam avtomatsko naredi */
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  socket.on('uporabniki',function(uporabniki){
    var seznam=$("#seznam-uporabnikov").val();
    for(var i=0;i<seznam.length;i++){
      seznam=seznam.split(uporabniki.prejsnjiVzdevek);
      console.log(seznam[i]);
    }
    /* ...bila je huda mravljica ...
    $("#seznam-uporabnikov").empty(); 
    $("#seznam-uporabnikov").text(divElementHtmlTekst(seznam));
    */
  })

  setInterval(function() {/* nesmiselno ampak .... vsako sekundo refresha socket kanali, ker nikoli ne uporabljemo tega socketa*/
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {/*koda za gumb poslji: prebere tekst in se posreduje naprej na streznikgit  */
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});