var Klepet = function(socket) {/* definiramo klepet ,*/
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {/* opredelitev funkcijo v razredu, skreira jason sporocilo in ga poslje na kanal socket.emit sporocila*/
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) { /* posljemo zahtevo*/
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) { /*prebere input fielda in ugotovi ... */
  var besede = ukaz.split(' '); /* preberemo input stream in uganemo ali se zacne s slash*/
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev': /* spremenimo kanal in klicemo metodo */
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':/* */
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};