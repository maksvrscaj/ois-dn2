//Na strani streznika
var socketio = require('socket.io'); //knjiznica
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = []; //nekdo ne more bit nekaj kar ze obstaja
var trenutniKanal = {};
var trenutniVzdevek='';
var fs=require("fs");
var seznamVulgarnih={};

exports.listen = function(streznik) {//s tem pozenemo streznik
  io = socketio.listen(streznik);
  io.set('log level', 1); //logiranje omejimo na konzolo, karkoli izpisujemo ven izpisujemo na konzolo streznika
  io.sockets.on('connection', function (socket) { //kako se obdela povezava
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);//dodelimo stevilko gostu
    pridruzitevKanalu(socket, 'Skedenj');//pridruzimo ga privzetemu kanalu, kamor padejo vsi  uporabniku
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);//kako se posiljajo sporocila
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {//na socket kanali sporocamo kateri kanali so aktivni in prikazujemo seznam kanalov
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);//ce se uporabnik odjavi ga izbrisemo
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {//vsi nasi gosti imajo Gost plus zaporedna stevilka
  var vzdevek = 'Gost' + stGosta;

  trenutniVzdevek=vzdevek;

  vzdevki[socket.id] = vzdevek;//shranimo med vzdevke
  socket.emit('vzdevekSpremembaOdgovor', {//nekdo ko se je prijavil na klepetalnico posljemo odgovor
    uspesno: true,
    vzdevek: vzdevek,//--------------------Spremeni
    kanal: trenutniKanal[socket.id]
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;//povecamo stevilo gostov
}

function pridruzitevKanalu(socket, kanal) {//privzeto se klice, ko se prijavimo na stran in nas sistem prijavi v skedenj
  socket.join(kanal);//pridruzimo se socketu
  trenutniKanal[socket.id] = kanal;//nastavimo spremenljivko

  socket.emit('pridruzitevOdgovor', {kanal: kanal,vzdevek: trenutniVzdevek});//-----------------------Spremeni

  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.' //vsem ki so se prijavili na kanalu posjemo sporocilo
  });


  var uporabnikiNaKanalu = io.sockets.clients(kanal); //43 56 dobimo vse uporabniku na kanalu in izpisemo povzetek, za nalogo imamo da bo ta aseznam na voljo v spodnjem delu seznama kanalov

  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId]; // zgeneriramo povzetek
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) { //
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) { // ce mi zahtevamo z nekaj kar se zacne z gost. to so rezervirani vzdevki
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else { 
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {//preverimo ce je ze uporabljen vzdevek
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);//nardimo novega, starega izbrisemo in seznama uporabnikov
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];

        trenutniVzdevek=vzdevek;

        socket.emit('vzdevekSpremembaOdgovor', {//vrnemo uspesno spremembo
          uspesno: true,
          vzdevek: vzdevek,//------------------------Spremeni
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {//vsem ostalim na kanalu posljemo sporocilo, da se je nekdo preimenoval
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
         socket.emit('uporabniki', {
              prejsnjiVzdevek: prejsnjiVzdevek,
             uporabniki: vzdevek
         });
      } else { //ce je vzdevek ze v uporabi
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) { //
  socket.on('sporocilo', function (sporocilo) {

      socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
   
  });
}



function obdelajPridruzitevKanalu(socket) { 
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}


function obdelajOdjavoUporabnika(socket) {//odjavimo uprobanika in izbrisemo iz vseh nickov, vzdevkov
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}